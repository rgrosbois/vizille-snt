import random
import re

def define_env(env):
    "Hook function"

    env.variables['term_counter'] = 0
    env.variables['REPL_counter'] = 0
    env.variables['dnd_counter'] = 0

    @env.macro
    def rg_dnd(question, liste, mel_prop=1, mel_empl=1) -> str:
        """
        Question de type glisser-déposer (Drag-and-Drop).

        La fonction génère le code HTML correspond à l'arbre suivant:
        * fieldset
        |-- legend (énoncé)
        |   |-- button (validation)
        |   |-- button (reset)
        |-- div (énoncé de la question)
        |-- div (pile de propositions)
        |   |-- div (proposition)
        |   |-- div (proposition)
        |   |-- ...
        |-- div (pile d'emplacements)
            |-- div (emplacement)
            |-- div (emplacement)
            |-- ...
        """

        html = f'<div class="enonce">{question}</div>'
        html += '<div class="pile-propositions">'

        # Liste de propositions (éventuellement mélangées)
        liste2 = liste.copy()
        if mel_prop==1:
            random.shuffle(liste2) 
        for src,dst in liste2:
            if src != '':
                html += f'<div class="proposition" id="i{env.variables["dnd_counter"]}" draggable="true" ondragstart="rg_dragstart(event)">{src}</div>'
                env.variables['dnd_counter'] += 1
            
        html += '</div><div class="pile-emplacements">'

        # Liste d'emplacements (éventuellement mélangés)
        if mel_empl==1:
            random.shuffle(liste) 
        for src,dst in liste:
            if dst != '':
                html += f'<div class="emplacement" ondragover="rg_dragover(event)" ondrop="rg_drop(event, \'{src}\')">{dst}</div>'

        html += '</div></fieldset>'

        dico = { key:val for val,key in liste}
        
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="corriger_dnd(this.parentNode.parentNode, {str(dico)})">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="reset_dnd(this.parentNode.parentNode, {str(dico)})">&#x21bb;</button></legend>'''

        return htmlStart + html

    @env.macro
    def rg_qcm(question: str, liste: list[str]) -> str:
        """
        Créer une question à choix multiple (une ou plusieurs bonne réponses sont possibles).

        La fonction génère le code HTML correspondant à l'arbre suivant :
        * fieldset
        |-- legend
        |   |-- button (validation)
        |   |-- button (reset)
        |-- pre (uniquement si [verbatim]...[/verbatim] dans l'énoncé)
        |-- div (énoncé de la question)
        |-- figure (uniquement si ![width=xx]nomfichier.xx! dans l'énoncé)
        |   |-- img
        |-- ul (propositions de réponses)
            |-- li
            |-- li
            |-- ...
        """
        # Mélanger l'ordre des réponses
        melange = ([i for i in range(len(liste))])
        random.shuffle(melange)        
        bonnes_reponses = '['+','.join(['true' if liste[melange[i]][0]=='+' else 'false' for i in range(len(liste))])+']'

        # Légendes (boutons JavaScript)
        correct_clic = f'correction_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        reset_clic = f'reinitialiser_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="{correct_clic}">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="{reset_clic}">&#x21bb;</button></legend>'''

        ################
        # Énoncé de la question

        # Mode horizontal ?
        i_horiz = question.find('[horiz]')
        if i_horiz!=-1:
            horizontal = True
            question = question[i_horiz+len('[horiz]'):]
        else:
            horizontal = False

        # Code verbatim
        i_verb_start = question.find('[verbatim]')
        i_verb_end = question.find('[/verbatim]')
        if i_verb_start!=-1 and i_verb_end!=-1:
            tmp = question[:i_verb_start].rstrip()
            tmp += '<pre>'
            tmp += question[i_verb_start+len('[verbatim]'):i_verb_end].strip().replace('\n', '\n    ')
            tmp += '</pre>'
            tmp += question[i_verb_end+len('[/verbatim]'):]
            question = tmp

        # Image
        m = re.search(r"!\[[\w]+=(?P<largeur>\w+)\](?P<fichier>[^!]+)!", question)
        if m:
            tmp = question[:m.start()].rstrip()
            tmp += question[m.end():].lstrip()
            question = tmp

            htmlStart += '<div class="enonce">'+question.strip()+'</div>'
            htmlStart += '<figure style="display: flex; justify-content: center;"><img src="'+m.group('fichier')+'" style="width: '+m.group('largeur')+';"></figure>'
        else:
            htmlStart += '<div class="enonce">'+question.strip()+'</div>'

        ################
        # Propositions de réponses

        # Affichage (mélangé) des propositions
        ul = '<ul class="reponses">'
        for i in range(len(liste)):
            elem = liste[melange[i]][1:]
            if horizontal:
                ul += f'<li onclick="cocher(event)" style="display: inline;">{elem}</li>'
            else:
                ul += f'<li onclick="cocher(event)">{elem}</li>'
        ul += '</ul>'

        # Bouton de validation
        htmlEnd = '</fieldset>'

        resultat = htmlStart + ul + htmlEnd
        return resultat
    
    def ajouter_question(questions: list[tuple], enonce: str, propositions: list[str]):
        """
        Rajouter une question (=énonce + propositions) dans un questionnaire.
        """

        if len(propositions)==0: # question non valide
            return

        # Caractères spéciaux
        enonce = enonce.replace("'", "′")
        enonce = enonce.replace('"', " ")
        for i in range(len(propositions)):
            propositions[i] = propositions[i].replace("'", "′")
            propositions[i] = propositions[i].replace("'", " ")

        # Ajouter au questionnaire
        questions.append( (enonce, propositions) )

    @env.macro
    def rg_qnum(reponse, precision) -> str:
        """
        Créer une question attendant une réponse numérique.
        """
        largeur = 3+len(str(reponse))

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="number" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qnum(this.parentNode, {reponse}, {precision});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qnum(this.parentNode, {reponse}, {precision})">&#10003;</button>'
        tmp += '</span>'
        return tmp

    @env.macro
    def rg_qsa(reponses: list[str]) -> str:
        """
        Créer une question attendant une réponse textuelle courte.
        """
        # Prévoir une zone de saisie de 3 caractères de plus
        # que la réponse la plus longue
        max = 0
        for r in reponses:
            if len(r)>max:
                max = len(r)
        largeur = 3+max

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="text" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qsa(this.parentNode, {reponses});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qsa(this.parentNode, {reponses})">&#10003;</button>'
        tmp += '</span>'
        return tmp


    @env.macro
    def rg_generer_qcm(nom_fichier) -> str:
        """
        Créer un questionnaire à partir d'un fichier au format ressemblant à l'AMC-TXT.
        """
        questions = []
        enonce = ''
        propositions = []

        # Extraire du fichier la liste des questions et leurs propositions
        with open(nom_fichier, 'rt') as f:
            continuer = True
            while continuer:
                ligne = f.readline()

                if ligne=='': # fin de fichier
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []
                    break

                ligne = ligne.strip() # supprimer la fin de ligne
                if ligne=='': # fin de question
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []

                elif ligne[0]=='+' or ligne[0]=='-': # proposition de réponse
                    propositions.append(ligne)
                else: # ligne d'énoncé de question
                    idx = 0
                    while ligne[idx]=='*': # Supprimer les * en début de ligne
                        idx += 1
                    if len(enonce)==0:
                        enonce = ligne[idx:]
                    else:
                        enonce += '\n'+ligne[idx:]

        # Générer le code HTML
        code_html = ''

        counter = 0
        for q in questions:
            counter += 1
            code_html += '=== "Q' + str(counter) + '"\n\t'
            code_html += rg_qcm(q[0], q[1]) + "\n"

        return code_html    
    
    @env.macro
    def rg_corriger_page(titre="Corriger toute la page") -> str:
        """
        Créer un bouton permettant de lancer l'opération de correction
        de tous les exercices de la page Web.
        """
        return f'<button class="md-button md-button--primary" onclick="corriger_page(this)">{titre}</button>'

    @env.macro
    def rgpopup(titre, contenu) -> str:
        tmp = f'<button class="md-button" onclick="this.parentNode.nextSibling.style.display = \'block\';">{titre}</button>'
        tmp += '<div class="rgmodal" onclick="this.style.display = \'none\';"><div class="rgmodal-content" onclick="event.stopPropagation();">'
        tmp += '<span class="fermer" onclick="this.parentNode.parentNode.style.display = \'none\';">&times;</span>'
        tmp += f'<h1>{titre}</h1><p>{contenu}</p></div></div>'
        return tmp
        
    @env.macro
    def terminal() -> str:
        """   
        Purpose : Create a Python Terminal.
        Methods : Two layers to avoid focusing on the Terminal. 
        1) Fake Terminal using CSS 
        2) A click hides the fake terminal and triggers the actual Terminal.
        """        
        tc = env.variables['term_counter']
        env.variables['term_counter'] += 1
        return f"""<div onclick='start_term("id{tc}")' id="fake_id{tc}" class="terminal_f"><label class="terminal"><span>>>> </span></label></div><div id="id{tc}" class="hide"></div>"""

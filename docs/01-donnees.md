# Les données structurées et leur traitement

## I. Généralités 

### 1. Quiz

{{ rg_generer_qcm('docs/01-quiz_intro.txt') }}

### 2. Vidéo

<div class="video-wrapper">
  <iframe width="100%" height="500" src="https://www.youtube.com/embed/IJJgcZ2DEs0" frameborder="0" allowfullscreen></iframe>
</div>

=== "Questions : "

=== "Donnée"

    {{ rg_qcm("Qu'est-ce qu'une donnée ?", 
	["- Un nombre entre 1 et 100.", 
	"- le nom d'un objet", 
	"+ Une valeur décrivant l'objet."]) }}

=== "Descripteur"

    {{ rg_qcm("Qu'est-ce qu'un descripteur ?", 
	["+ Une catégorie.", 
	"- La taille d'un objet", 
	"- Un texte donnant des informations sur un objet."]) }}

=== "Métadonnées"

    {{ rg_qcm("À quoi servent les métadonnées ?", 
	["- À enregistrer des données secrètes.", 
	"- À enregistrer des données très volumineuses", 
	"+ À décrire le contenu d'un fichier."]) }}

=== "RGPD"

    {{ rg_qcm("Quel est l'objectif du RGPD ?", 
	["- Lutter contre les fake news.", 
	"+ Protéger les données personnelles et préserver l'anonymat", 
	"- Protéger les droits d'auteurs."]) }}


### 3. Repères historiques

=== "1725" 
    ![Métier à tisser](img/01-ruban.png){ align=left width=55% }

    !!! info inline end
        Le français Basile Bouchon programme un métier à tisser 
		grâce à un ruban perforé.
		
		(codage d'instructions en binaire)
		
=== "1928"
	![Carte perforée](img/01-carte_perforee.jpg){align=left width=55% }
	
	!!! info inline end
		La société IBM brevète la carte perforée à 80 colonnes.
		
		(mémoire de masse binaire)
		
=== "1956"
	![Disque dur](img/01-hdd.jpg){align=left width=55% }
	
	!!! info inline end
		1er disque dur magnétique (IBM 350): 
		
		- ~1 tonne, 
		- 5 Mo, 
		- 1 200 tr/min,
		- 50 disques de 24 pouces, 
		- 2 têtes de lecture/écriture, 
		- 8.8 ko/s.

=== "1970"
	![Edgar Codd](img/01-E_Codd.jpg){align=left width=55% }
	
	!!! info inline end
		Edgar Codd (IBM) invente le modèle relationnel.

=== "1979"
	![Visicalc](img/01-Visicalc.png){align=left width=55% }
	
	!!! info inline end
		Dan Bricklin créé le tableau Visicalc.

=== "1997"
	Big Data
	
=== "2009"
	Open Data (charte du G8 en 2013)
	
## II. Données simples

Une **donnée** est une valeur numérique (*binaire*) qui permet un
traitement informatique.

### 1. Représentation informatique

#### a. Des nombres

Exemple de la représentation d'un entier naturel sur 8 bits (=1 octet)

![Décomposition octet](img/01-octet.svg){ width=60% }

{{ rg_dnd('Retrouver la représentation décimale de chaque octet', 
	[ 
		('00110010' , '50'),
		('' , '52'),
		('00100000' , '32'),
		('' , '31'),
		('01101001' , '105'),
		('01101110' , '110'),
		('' , '111'),
	]) }}

#### b. Du texte

Exemple des codes ASCII sur 7 bits

![Table ASCII](img/01-ASCII.png){ width=50% }

=== "Q1"
	{{ rg_dnd('Quels codes correspondent aux caractères `S`, `N` et `T` ?',
	[ 
	('S', '83' ),
	('N', '78' ),
	('T', '84' ),
	('' , '55' ),
	('' , '81' ),
	('' , '80' ),
	('' , '75' ),
	('' , '45' ),
	], 0, 1 ) }}

	??? info "Détails"
	    Les octets correspondants sont: `01010011`, `01001110` et `01010100`.

=== "Q2"

	{{ rg_dnd('Retrouver les caractères représentés par ces octets', 
	[ 
		('V' , '01010110'),
		('I' , '01001001'),
		('Z' , '01011010'),
		('I' , '01001001'),
		('L' , '01001100'),
		('L' , '01001100'),
		('E' , '01000101'),
		('A', ''),
		('B', ''),
		('C', ''),
		('D', ''),
		('F', ''),
		('G', ''),
	], 1, 0) }}

#### c. Autre exemple


### 2. Utilisation du langage Python

La fonction `type` permet de connaître le type de donnée utilisé :
```python
type(156)
type(2.5)
type("Sciences Numériques et Technologie")
```

> terminal()

??? info "Types de données"
    - Un nombre est de type entier (`int`) ou flottant (`float`). 
    - Le texte est de type chaîne de caractère (`str`) où chaque caractère 
    est codé en UTF-8.

Pour mémoriser ou réutiliser une donnee, on peut la stocker dans une variable:
```python
nombre1 = 156
nombre2 = 2.5
texte1 = "Sciences Numériques et Technologie"
```

> terminal()

??? tip "horodatage"
    Le temps *UNIX* correspond au nombre de secondes écoulé depuis le 
    1er janvier 1970 à 00:00:00 GMT.

    ```python
	from datetime import datetime, tzinfo, timezone
	
	# Initialisation au 3 septembre 2021 à 15:30:00
	dt = datetime(2021, 9, 3, 15, 30, 0, tzinfo=timezone.utc)
	
	# Nombre de secondes écoulé
	dt.timestamp()
	```
    
	> terminal()

1. [Les données structurées et leur traitement](01-donnees.md)
    - [QCM final](01-qcm.md)

2. Internet
3. Le Web
4. Localisation, cartographie et mobilité
5. La photographie numérique
6. Les réseaux sociaux
7. Informatique embarquée et objets connectés

